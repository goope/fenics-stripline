import numpy as np
import dolfin

def evaluate_over_grid(u, xs, ys, zs):
    xx, yy, zz = np.meshgrid(xs, ys, zs)
    nx, ny, nz = len(xs), len(ys), len(zs)
    x = xx.reshape((1, nx*ny*nz))
    y = yy.reshape((1, nx*ny*nz))
    z = zz.reshape((1, nx*ny*nz))
    points = np.zeros((nx*ny*nz, 3))
    points[:,0] = x
    points[:,1] = y
    points[:,2] = z
    v = evaluate_over_points(u, points)
    n, dims = v.shape
    return v.reshape((nx, ny, nz, dims))

def evaluate_over_points(u, points):
    dims = u.value_size()
    values = np.zeros((len(points), dims))
    for i, x in enumerate(points):
        values[i,:] = u(x)
    return values

def contour_line(d=[0,0,0], direction='x'):
    c = {'x': lambda t: np.array(d) + np.array([t, 0, 0]),
         'y': lambda t: np.array(d) + np.array([0, t, 0]),
         'z': lambda t: np.array(d) + np.array([0, 0, t])}
    return c[direction]

def contour_circle(d=[0,0,0], r=1.0, direction='x'):
    c = {'x': lambda t: np.array(d) + r*np.array([0, np.cos(t), np.sin(t)]),
         'y': lambda t: np.array(d) + r*np.array([np.cos(t), 0, np.sin(t)]),
         'z': lambda t: np.array(d) + r*np.array([np.cos(t), np.sin(t), 0])}
    return c[direction]

def radial_versor(c=[0, 0, 0]):
    c = np.array(c)
    class RadialVersor(dolfin.Expression):
         def eval(self, values, x):
             r = (x - c)
             R = np.linalg.norm(r)
             values[:] =  r/R
         def value_shape(self):
             return (3,)

    return RadialVersor(degree=1)
 
def tangential_versor_xz(c=[0, 0, 0]):
    c = np.array(c)
    class TangentialVersor(dolfin.Expression):
         def eval(self, values, x):
             r = (x - c)
             R = np.linalg.norm(r)
             radial    =  r/R
             values[0] = -radial[2]
             values[1] = radial[1]
             values[2] = +radial[0]

         def value_shape(self):
             return (3,)

    return TangentialVersor(degree=1)
 
def tangential_versor_yz(c=[0, 0, 0]):
    c = np.array(c)
    class TangentialVersor(dolfin.Expression):
         def eval(self, values, x):
             r = (x - c)
             R = np.linalg.norm(r)
             radial    =  r/R
             values[0] = radial[0]
             values[1] = -radial[2]
             values[2] = +radial[1]

         def value_shape(self):
             return (3,)

    return TangentialVersor(degree=0)
 
def contour_to_points(start, end, contour, npoints):
    ts = np.linspace(start, end, npoints + 1)
    points = np.array([contour(t) for t in ts])
    return points, ts

def evaluate_over_contour(u, start=0, end=2*np.pi, contour=lambda t: [np.cos(t), np.sin(t), 0], npoints=100):
    points, _ = contour_to_points(start, end, contour, npoints)
    return evaluate_over_points(u, points)

def intergrate_over_contour(u, start=0, end=2*np.pi, contour=lambda t: [np.cos(t), np.sin(t), 0], npoints=100):
    dt = (end - start)/npoints
    points, ts = contour_to_points(start, end, contour, npoints)
    values = evaluate_over_points(u, points)
    last_point = points[-1]
    extra_point = np.array(contour(end + dt))

    dl = np.diff(points, axis=0)
    dl = np.append(dl, np.array([extra_point - last_point]), axis=0)

    x = numpy.linalg.norm(dl, axis=1)
    y = np.sum(values * dl, axis=1)
    return scipy.integrate.simps(y/x, x * ts / dt)

def integrate_over_values(values):
    dt = (end - start)/npoints
    points, ts = contour_to_points(start, end, contour, npoints)
    last_point = points[-1]
    extra_point = np.array(contour(end + dt))

    dl = np.diff(points, axis=0)
    dl = np.append(dl, np.array([extra_point - last_point]), axis=0)

    x = numpy.linalg.norm(dl, axis=1)
    y = np.sum(values * dl, axis=1)
    return scipy.integrate.simps(y/x, x * ts / dt)
