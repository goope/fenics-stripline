cl__1 = 1;
// Basic geometry
Point(01) = {0, 0, 0, 1};
Point(02) = {0.0015, 0, 0, 1};
Point(03) = {0, 0.0015, 0, 1};
Point(04) = {0.00475, 0, 0, 1};
Point(05) = {0, 0.00475, 0, 1};
Point(06) = {-0.0015, 0, 0, 1};
Point(07) = {-0.00475, 0, 0, 1};
Point(08) = {0, -0.0015, 0, 1};
Point(09) = {0, -0.00475, 0, 1};
Circle(1) = {2, 1, 3};
Circle(2) = {4, 1, 5};
Circle(3) = {3, 1, 6};
Circle(4) = {5, 1, 7};
Circle(5) = {6, 1, 8};
Circle(6) = {7, 1, 9};
Circle(7) = {8, 1, 2};
Circle(8) = {9, 1, 4};
Line(9) = {2, 4};
Line(10) = {3, 5};
Line(11) = {6, 7};
Line(12) = {8, 9};
Line Loop(13) = {1, 10, -2, -9};
Plane Surface(14) = {13};
Line Loop(15) = {3, 11, -4, -10};
Plane Surface(16) = {15};
Line Loop(17) = {5, 12, -6, -11};
Plane Surface(18) = {17};
Line Loop(19) = {7, 9, -8, -12};
Plane Surface(20) = {19};
Extrude {0, 0, 0.1} {
  Surface{16};
}
Extrude {0, 0, 0.1} {
  Surface{14};
}
Extrude {0, 0, 0.1} {
  Surface{20};
}
Extrude {0, 0, 0.1} {
  Surface{18};
}
Physical Surface(1) = {42, 64, 86, 108};
Physical Surface(2) = {16, 14, 20, 18};
Physical Surface(3) = {37, 59, 81, 103};
Physical Surface(4) = {51, 73, 95, 29};
Physical Volume(1) = {1, 2, 3, 4};
