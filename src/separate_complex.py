from sympy import Function, symbols, re, im, simplify, init_printing, pprint
from sympy.printing.python import PythonPrinter, StrPrinter
from sympy.core.sympify import converter

from sympy import *

class S2UPrinter(PythonPrinter):
    def __init__(self, skip_multiplication_by_one=False):
        self.skip_multiplication_by_one = skip_multiplication_by_one
        PythonPrinter.__init__(self)

    def _print_Float(self,expr):
        return str(expr)

    def _print_Function(self, expr):
        if expr.func == re:
            arg = expr.args[0]
            if hasattr(arg, 'name'):
                return arg.name + '_re'
            return str(arg.func) + '(' + ','.join([self._print(x) for x in arg.args]) + ')'
        if expr.func == im:
            arg = expr.args[0]
            if hasattr(arg, 'name'):
                return arg.name + '_im'
            return str(arg.func) + '(' + ','.join([self._print(x) for x in arg.args]) + ')'
        return PythonPrinter._print_Function(self, expr)

    def _print_Symbol(self, expr):
        return expr.name

    def _print_Rational(self,expr):
        return StrPrinter._print_Rational(self,expr)

    def _print_Mul(self,expr):
        def rearrange_multiplication(expr):
            sign = ''
            if expr.startswith('-'):
                expr = expr[1:]
                sign = '-'
            measures = []
            functions = []
            numbers = []
            symbols = []

            args = expr.split('*')
            for arg in args:
                if arg.startswith('d'):
                    measures.append(arg)
                elif arg.find('('):
                    functions.append(arg)
                elif arg.find('.'):
                    numbers.append(arg)
                else:
                    symbols.append(arg)
            return sign + '*'.join(numbers + symbols + functions + measures)
        return rearrange_multiplication(StrPrinter._print_Mul(self, expr)).replace('.00000000000000','')

class ComplexFunction(Function):
    @classmethod
    def eval(cls, *args):
        for arg in args:
            if arg.is_Number and arg is S.Zero:
                return S.Zero

    def _eval_is_real(self):
        for arg in self.args:
            if not arg.is_real:
                return False
        return True
        
class grad(ComplexFunction):
    nargs = 1
    def as_real_imag(self, deep=True, **hints):
        u_re, u_im = self.args[0].as_real_imag()
        return (grad(u_re), grad(u_im))

class div(grad):
    nargs = 1
    def as_real_imag(self, deep=True, **hints):
        u_re, u_im = self.args[0].as_real_imag()
        return (div(u_re), div(u_im))

class curl(grad):
    nargs = 1
    def as_real_imag(self, deep=True, **hints):
        u_re, u_im = self.args[0].as_real_imag()
        return (curl(u_re), curl(u_im))

class nabla_grad(grad):
    nargs = 1
    def as_real_imag(self, deep=True, **hints):
        u_re, u_im = self.args[0].as_real_imag()
        return (nabla_grad(u_re), nabla_grad(u_im))

class rot(curl):
    pass

class inner(ComplexFunction):
    nargs = 2
    treat_arguments_as_vectors = True

    @classmethod
    def eval(cls, u, v):
        if not cls.treat_arguments_as_vectors and u.is_real and v.is_real:
            return u*v
        return ComplexFunction.eval(cls, u, v)

    def as_real_imag(self, deep=True, **hints):
        u_re,u_im = self.args[0].as_real_imag()
        v_re,v_im = self.args[1].as_real_imag()
        return (inner(u_re,v_re) - inner(u_im,v_im), inner(u_re,v_im) + inner(v_re,u_im))

class cross(ComplexFunction):
    nargs = 2
    def as_real_imag(self, deep=True, **hints):
        u_re,u_im = self.args[0].as_real_imag()
        v_re,v_im = self.args[1].as_real_imag()
        return (cross(u_re,v_re) - cross(u_im,v_im), cross(u_re,v_im) + cross(v_re,u_im))

def separate(form_as_a_string, **kwargs):
    for var_name, var in kwargs.iteritems():
        if type(var) is tuple: # as complex value: (real, imaginary)
            locals()[var_name] = symbols(var_name)
        else: # as real value: value
            locals()[var_name] = symbols(var_name, real=True)

    form_as_expression = eval(' '.join(form_as_a_string.split()))
    real_part_of_expression, imaginary_part_of_expression = form_as_expression.expand(complex=True).as_real_imag()
    form_printer = S2UPrinter()

    try:
        from dolfin import cross, curl, grad, inner
    except ImportError:
        from firedrake import cross, curl, grad, inner

    for var_name, var in kwargs.iteritems():
        if type(var) is tuple: # as complex value: (real, imaginary)
            locals()[var_name + '_re'] = var[0]
            locals()[var_name + '_im'] = var[1]
        else: # as real value: value
            locals()[var_name] = var

    print "Debug: real part of the expression after separation:\n\t%s" % form_printer.doprint(real_part_of_expression)
    print "Debug: imaginary part of the expression after separation:\n\t%s" % form_printer.doprint(imaginary_part_of_expression)
    real_part_as_ufl = eval(form_printer.doprint(real_part_of_expression))
    imaginary_part_as_ufl = eval(form_printer.doprint(imaginary_part_of_expression))
    return real_part_as_ufl, imaginary_part_as_ufl

if __name__ == '__main__':
    T, E = symbols('T, E')
    mur = symbols('mur', real=True)
    a_re, a_im = separate("mur*T*E", T=('T_re','T_im'), E=('E_re','E_im'))
    #u, v = symbols('u,v')
    #print (inner(u, v)).expand(complex=True)
    #print (cross(u, v)).expand(complex=True)
    #print (grad(u)).expand(complex=True)
    #print (curl(u)).expand(complex=True)
    
    #a,b,c,d = symbols('a,b,c,d', real=True)
    #u = a + 1j*b
    #v = c + 1j*d

    #inner.treat_arguments_as_vectors = False
    #print inner(u, v.conjugate()).expand(complex=True)
    #print inner(u, v.conjugate()).expand(complex=True).subs(a, 2).subs(b, 1).subs(c, 1).subs(d, 1)
    #inner.treat_arguments_as_vectors = True
    
    #u1,u2,u3 = symbols('u1,u2,u3')
    #v1,v2,v3 = symbols('v1,v2,v3')
    
    #init_printing()
    #print "1j*k0*Z0*inner(Jimp, T)*dV = "
    #pprint(L_re, order=None)
    #pprint(a_re, order=None)
    #print(a_re)
    #print(L_re)

    #u,v = symbols('u,v')
    #f = symbols('f')
    #rho,k = symbols('rho,k', real=True)
    #dx,ds = symbols('dx,ds', real=True)
    #a = \
    #+ 1/rho*inner(grad(u),grad(v))*dx\
    #- k**2*u*v*dx\
    #- f*v*dx\
    #- 1j*k*u*v*ds\

    #a_re, a_im = a.expand(complex=True).as_real_imag()
    #print S2UPrinter().doprint(a_re)
