# coding: utf-8
#
# Solving Helmhotz equation with FEniCS
# Author: Juan Luis Cano Rodríguez <juanlu@pybonacci.org>
# Inspired by: http://jasmcole.com/2014/08/25/helmhurts/
#
import sys
import numpy as np
import separate_complex
from math import pi
from sympy import *
from dolfin import *

class CoaxialElectricField(Expression):
    def __init__(self, A, R0, R1):
        self.A = A
        self.R1 = R1
        self.R0 = R0
        Expression.__init__(self)

    def eval(self, values, x):
        A = self.A

        R = sqrt(x[0]**2+x[1]**2)
        R0 = self.R0
        R1 = self.R1

        E = (np.log(R1)-np.log(R))/np.log(R1/R0)

        r = x/x.dot(x)

        values[:] = -A*E*r

    def value_shape(self):
        return (3,)

## Problem data
epsr_values = [1.00, 2.04]
mur_values  = [1.00, 1.00]
freq = Constant(100e6)
omega = 2*pi*freq
c = Constant(299792458.0)
mu0 = Constant(4*pi*1e-7)
eps0 = Constant(1.0 / (mu0*c**2))
r0 = 1.5e-3
r1 = 4.5e-3
k0 = omega/c
Z0 = Constant(377.0)#sqrt(mu0/eps0)
## Formulation
mesh = Mesh("mesh/c-mesh.xml")
volumes = MeshFunction('size_t', mesh, "mesh/c-mesh_physical_region.xml")
surfaces = MeshFunction('size_t', mesh, "mesh/c-mesh_facet_region.xml")
n = FacetNormal(mesh)
## Surface identifiers
input_port = 1
output_port = 2
outer_conductor = 3
inner_conductor = 4
## Boundary identifiers
input_port_boundary = 1
output_port_boundary = 2
pec_boundary = 3
## Map surfaces to boundaries
boundaries = FacetFunction("size_t", mesh, 0)
boundaries.array()[surfaces.array()==outer_conductor] = pec_boundary
boundaries.array()[surfaces.array()==inner_conductor] = pec_boundary
boundaries.array()[surfaces.array()==input_port]  = input_port_boundary
boundaries.array()[surfaces.array()==output_port] = output_port_boundary

V_re = FunctionSpace(mesh, "Nedelec 1st kind H(curl)", 2)
V_im = FunctionSpace(mesh, "Nedelec 1st kind H(curl)", 2)
V = V_re * V_im
V0 = FunctionSpace(mesh, "DG", 0)
markers = np.asarray(volumes.array(), dtype='int32')

E_re, E_im = TrialFunctions(V)
T_re, T_im = TestFunctions(V)
Jimp_re, Jimp_im = Expression(("0.0", "0.0", "10.0"), degree=1), Expression(("0.0", "0.0", "0.0"), degree=1)

mur = Function(V0)
epsr = Function(V0)
mur.vector()[:] = np.choose(markers, mur_values)
epsr.vector()[:] = np.choose(markers, epsr_values)

dV = Measure('cell')
dS0 = Measure('exterior_facet')[boundaries](output_port_boundary)
dSPEC = Measure('exterior_facet')[boundaries](pec_boundary)

## Assemble system
a = \
+ Z0*k0*inner(Jimp_re, T_im)*dx + Z0*k0*inner(T_re, Jimp_im)*dx\
+ (1./(mur))*inner(cross(n, T_re), cross(n, E_re))*dSPEC - (1./(mur))*inner(cross(n, T_im), cross(n, E_im))*dSPEC\
- epsr*k0**2*inner(T_re, E_re)*dx + epsr*k0**2*inner(T_im, E_im)*dx\
+ (1./(mur))*inner(curl(T_re), curl(E_re))*dx - (1./(mur))*inner(curl(T_im), curl(E_im))*dx\
+ k0*inner(cross(n, E_re), cross(n, T_im))*dS0 + k0*inner(cross(n, T_re), cross(n, E_im))*dS0\

A = assemble(lhs(a))
b = assemble(rhs(a))

## Boundary conditions
bcs = []#DirichletBC(V_re, CoaxialElectricField(10, r0, r1), boundaries, input_port_boundary)]
for bc in bcs:
    bc.apply(A, b)
## Solve system
E = Function(V)
solve(A, E.vector(), b, "mumps")
E_re, E_im = E.split()

file = File("E_re.pvd")
file << E_re
file = File("E_im.pvd")
file << E_im
file = File("volumes.pvd")
file << volumes
file = File("surfaces.pvd")
file << surfaces
file = File("boundaries.pvd")
file << boundaries
file = File("epsr.pvd")
file << epsr
file = File("mur.pvd")
file << mur
