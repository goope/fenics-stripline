# coding: utf-8
#
import numpy as np
import matplotlib.pyplot as plt
import separate_complex
from sys import argv
from dolfin.cpp.io import File
from dolfin.cpp.mesh import Mesh, FacetFunction
from dolfin.cpp.mesh import MeshFunction
from comsol import *
from export import *
from postprocessing import *
from emw import *
from dolfin import *

## Physical constants
freq = Constant(100e6)
omega = 2 * pi * freq
c = Constant(299792458.0)
mu0 = Constant(4 * pi * 1e-7)
eps0 = Constant(1.0 / (mu0 * c ** 2))
r0 = 1.5e-3
r1 = 4.75e-3
k0 = omega / c
Z0 = sqrt(mu0/eps0)

## Problem data
sigma_values = [0.00, 0.00, 0.00, 0.00]
epsr_values =  [1.00, 2.04, 1.00, 2.15]
mur_values =   [1.00, 1.00, 1.00, 1.00]
## Formulation
mesh_name = argv[1]

print "Reading %s" % mesh_name
mesh = Mesh()
hdf = HDF5File(mesh.mpi_comm(), "mesh/" + mesh_name + ".h5", "r")
hdf.read(mesh, "/mesh", False)
print "Done reading the mesh"

print "Reading subdomains"
subdomains = CellFunction("size_t", mesh)
hdf.read(subdomains, "/subdomains")
print "Reading boundaries"
boundaries = FacetFunction("size_t", mesh)
hdf.read(boundaries, "/boundaries")
print "Calculating normals"
n = FacetNormal(mesh)
File(mesh_name + "_volumes.pvd") << subdomains
File(mesh_name + "_surfaces.pvd") << boundaries
print "Building FunctionSpaces"
V_re = FunctionSpace(mesh, "N2curl", 1)
V_im = FunctionSpace(mesh, "N2curl", 1)
V = V_re * V_im
V0 = FunctionSpace(mesh, "DG", 0)
E_re, E_im = TrialFunctions(V)
T_re, T_im = TestFunctions(V)
# Surface identifiers
ids = expand_identifiers

input_port = np.in1d(boundaries.array(), ids('4'))
output_port = np.in1d(boundaries.array(), ids('7'))
outer_conductor = np.in1d(boundaries.array(), ids('3'))
inner_conductor = np.in1d(boundaries.array(), ids('2'))
ports_conductor = np.in1d(boundaries.array(), ids('5,6'))
absorbing_walls = np.in1d(boundaries.array(), ids('1'))

dielectric = np.in1d(subdomains.array(), ids('10'))
air        = np.in1d(subdomains.array(), ids('9'))
#steel      = np.in1d(subdomains.array(), ids('2-3, 6-7, 9, 11-13, 16, 18, 26, 28, 30-32, 35-37, 39, 41'))
#wood       = np.in1d(subdomains.array(), ids('14, 17, 24, 27'))
 
# Boundary identifiers
nothing              = 0
input_port_boundary  = 1
output_port_boundary = 2
absorbing_boundary   = 3
pec_boundary         = 4
# Subdomain identifiers
air_material        = 0
dielectric_material = 1
#steel_material      = 2
#wood_material       = 3
# Map surfaces to boundaries
boundaries = FacetFunction("size_t", mesh)
print "Marking boundaries"
boundaries.array()[:] = nothing
boundaries.array()[outer_conductor] = pec_boundary
boundaries.array()[inner_conductor] = pec_boundary
boundaries.array()[ports_conductor] = pec_boundary
boundaries.array()[input_port] = input_port_boundary
boundaries.array()[output_port] = output_port_boundary
boundaries.array()[absorbing_walls] = absorbing_boundary
# Map volumes to subdomains
subdomains = CellFunction("size_t", mesh)
print "Marking subdomains"
subdomains.array()[:] = air_material
subdomains.array()[dielectric] = dielectric_material
subdomains.array()[air] = air_material
#subdomains.array()[steel] = steel_material
#subdomains.array()[wood] = wood_material


markers = np.asarray(subdomains.array(), dtype='int32')

mur = Function(V0)
epsr = Function(V0)
sigma = Function(V0)
mur.vector()[:] = np.choose(markers, mur_values)
epsr.vector()[:] = np.choose(markers, epsr_values)
sigma.vector()[:] = np.choose(markers, sigma_values)
File(mesh_name + "_subdomains.pvd") << subdomains
File(mesh_name + "_boundaries.pvd") << boundaries
File(mesh_name + "_epsr.pvd") << epsr
File(mesh_name + "_sigma.pvd") << sigma
File(mesh_name + "_mur.pvd") << mur

#k  = k0 * sqrt( mur * (epsr + 1j * sigma * omega) )
k  = k0 * sqrt( mur * epsr )

print "Configuring measures"
dV = Measure('cell')
dSSBC = Measure('exterior_facet')[boundaries](absorbing_boundary)
dSPEC = Measure('exterior_facet')[boundaries](pec_boundary)
dSin  = Measure('exterior_facet')[boundaries](input_port_boundary)
dSout = Measure('exterior_facet')[boundaries](output_port_boundary)

freq_v = freq([0,0,0]) * 1e-6
print "Compiling the form for freq = %d MHz" % freq_v

helmholtz_equation = HelmholtzEquation(T_re, T_im, E_re, E_im, k0, epsr, mur, dV)
scattering_boundary = ScatteringBoundaryCondition(T_re, T_im, E_re, E_im, n, k, dSSBC)
helmholtz_equation.add(scattering_boundary)

waveguide_port = WaveguidePortBoundaryCondition(T_re, T_im, E_re, E_im,
  n=n, k=k, mur=mur, r0=r0, r1=r1, E0=1.0, c=(0.0, -2.38299, 0.013), dS=dSin)    
helmholtz_equation.add(waveguide_port)

waveguide_port = WaveguidePortBoundaryCondition(T_re, T_im, E_re, E_im,
  n=n, k=k, mur=mur, r0=r0, r1=r1, dS=dSout)    
helmholtz_equation.add(waveguide_port)

# Boundary conditions
pec = PerfectElectricConductor()
bcs = [
	DirichletBC(V.sub(0), pec, boundaries, pec_boundary),
	DirichletBC(V.sub(1), pec, boundaries, pec_boundary),
]

print "Assembling of the system of equations..."
a, L = helmholtz_equation.forms()
A, b = assemble_system(a, L)
for bc in bcs:
  bc.apply(A)
  bc.apply(b)
print "...done"

# Solve system
E = Function(V, name="Electric Field")
print "Solving..."
set_log_level(PROGRESS)
solve(A, E.vector(), b, 'mumps')
print "...done"

#Ev = Vector()
#E.vector().gather(Ev, range(V.dim()), 'intc')
#E.vector()[:] = Ev
E_re, E_im = E.split()
H_re, H_im = MagneticField(E_re, E_im, omega, mu0).eval()

print "Postprocessing..."
E_re.set_allow_extrapolation(True)
E_im.set_allow_extrapolation(True)
H_re.set_allow_extrapolation(True)
H_im.set_allow_extrapolation(True)

#c=(0.0, -0.913, 0.013)
x0 = 0
y0 = -2.38299
z0 = 0.013
line = lambda t: [t, y0, z0]
E  = evaluate_over_contour(E_re, start=1.5e-3, end=4.75e-3, contour=line) + 1j*evaluate_over_contour(E_im, start=1.5e-3, end=4.75e-3, contour=line)
dr = evaluate_over_contour(radial_versor([x0, y0, z0]), start=1.5e-3, end=4.75e-3, contour=line)
Edr = (E*dr).sum(axis=1)
tr = np.linspace(1.5e-3, 4.75e-3, 100+1)
print "calculated data for voltage"

circle = lambda t: np.array([x0, y0, z0]) + 2e-3*np.array([np.sin(t), 0, np.cos(t)])
H  = evaluate_over_contour(H_re, start=0, end=2*np.pi, contour=circle) + 1j*evaluate_over_contour(H_im, start=0, end=2*np.pi, contour=circle)
dl = evaluate_over_contour(tangential_versor_xz([x0, y0, z0]), start=0, end=2*np.pi, contour=circle)
Hdl = (H*dl).sum(axis=1)
tl = np.linspace(0, 2*np.pi*2e-3, 100+1)

V = scipy.integrate.simps(Edr, tr)
I = scipy.integrate.simps(Hdl, tl)
print "Z = %s/%s = %s" % (V, I, V/I)
El_re = evaluate_over_contour(E_re, start=-2.0, end=+2.0, contour=lambda t: np.array([0, t, 0.013]))
El_im = evaluate_over_contour(E_im, start=-2.0, end=+2.0, contour=lambda t: np.array([0, t, 0.013]))
Er_re = np.real(E)
Er_im = np.imag(E)
El = np.sqrt(El_re**2 + El_im**2)

f = ResultFile(mesh)
f.add_data(E_re, name='E -- real (%d)' % freq_v)
f.add_data(E_im, name='E -- imag (%d)' % freq_v)
f.add_data(H_re, name='H -- real (%d)' % freq_v)
f.add_data(H_im, name='H -- imag (%d)' % freq_v)
f.add_data(El_re, name='E(l) -- real (%d)' % freq_v)
f.add_data(El_im, name='E(l) -- imag (%d)' % freq_v)
f.add_data(El_re, name='E(l) -- real (%d)' % freq_v)
f.add_data(El_im, name='E(l) -- imag (%d)' % freq_v)
f.add_data(El, name='E(l) -- norm (%d)' % freq_v)
f.add_data(Er_re, name='E(r) -- real (%d)' % freq_v)
f.add_data(Er_im, name='E(r) -- imag (%d)' % freq_v)
f.write("result/" + mesh_name + ".vtu")
print "...done"
