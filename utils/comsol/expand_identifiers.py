def expand_identifiers(identifiers):
  expanded = []
  for identifier in identifiers.split(','):
    if '-' in identifier:
      start, end = identifier.split('-')
      try:
        for i in range(int(start), int(end)+1):
          expanded.append(i)
      except ValueError:
        print "Skipping range %s - %s" % (start, end)
        continue
    else:
      try:
        i = int(identifier)
        expanded.append(i)
      except ValueError:
        print "Skipping %s" % identifier
        continue
  return expanded
