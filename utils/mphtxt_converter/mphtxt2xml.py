# -*- coding: utf-8 -*-

# this is change

import os

import sys

import numpy

import xml_writer


def mphtxt2xml(ifilename, ofilename):
    print("Converting from COMSOL format (.mphtxt) to DOLFIN XML format")

    # Paths for physical and facet regions files
    facet_region_ofilename = os.path.splitext(ofilename)[0] + '_facet_region.xml'
    physical_region_ofilename = os.path.splitext(ofilename)[0] + '_physical_region.xml'

    # Open files
    ifile = open(ifilename, "r")
    ofile = open(ofilename, "w")
    f_ofile = open(facet_region_ofilename, "w")
    p_ofile = open(physical_region_ofilename, "w")

    # Data containers
    vertices = {}
    mesh_type_for_elements_dimension = {1: "interval", 2: "triangle", 3: "tetrahedron"}
    highest_dimension_elements = {'number_of_elements': 0, 'number_of_markers': 0, 'vertices_used_for_elements': [], 'markers_used_for_elements': []}
    lower_dimension_elements = {'number_of_elements': 0, 'number_of_markers': 0, 'vertices_used_for_elements': [], 'markers_used_for_elements': []}

    dim = 3
    state = 0
    number_of_element_types = 0

    # Reading states
    # 0 - waiting state
    # 1 - reading vertices/coordinates
    # 2 - reading lower_dimension elements
    # 3 - reading highest dimension elements
    # 4 - reading lower dimension elements markers
    # 5 - reading highest dimension elements markers

    for line in ifile:

        line = line.rstrip('\n\r')

        if not line:
            if state == 2:
                state = 4
            elif state == 3:
                state = 5
            else:
                state = 0
            continue

        if state == 0:
            if '# sdim' in line:
                dim = int(line.split()[0])
            if '# Mesh point coordinates' in line:
                state = 1

            elif '# number of element types' in line:
                number_of_element_types = int(line.split()[0])
                if number_of_element_types == 1:
                    _error('Unable to find cells of supported type.')

            elif str(number_of_element_types - 1) + ' # number of nodes per element' in line:
                state = 2

            elif str(number_of_element_types) + ' # number of nodes per element' in line:
                state = 3

        elif state == 1:
            vertices[len(vertices)] = [float(n) for n in line.split()]

        elif state == 2 and '#' not in line:
            lower_dimension_elements[len(lower_dimension_elements) - 4] = {
                'vertices_used_for_element': [int(n) for n in line.split()]}
            lower_dimension_elements['number_of_elements'] = len(lower_dimension_elements) - 4
            lower_dimension_elements['vertices_used_for_elements'].extend([int(n) for n in line.split()])

        elif state == 3 and '#' not in line:
            highest_dimension_elements[len(highest_dimension_elements) - 4] = {
                'vertices_used_for_element': [int(n) for n in line.split()]}
            highest_dimension_elements['number_of_elements'] = len(highest_dimension_elements) - 4
            highest_dimension_elements['vertices_used_for_elements'].extend([int(n) for n in line.split()])

        elif state == 4 and '#' not in line:
            lower_dimension_elements[lower_dimension_elements.get('number_of_markers')]['marker'] = int(line) + 1
            lower_dimension_elements['markers_used_for_elements'].append(int(line) + 1)
            lower_dimension_elements['number_of_markers'] += 1

        elif state == 5 and '#' not in line:
            highest_dimension_elements[highest_dimension_elements.get('number_of_markers')]['marker'] = int(line)
            highest_dimension_elements['markers_used_for_elements'].append(int(line))
            highest_dimension_elements['number_of_markers'] += 1

    vertices_used_for_highest_dimension_set = set(highest_dimension_elements.get('vertices_used_for_elements'))
    # highest_dimension_elements['vertices_used_for_elements'] = None

    highest_elements_dimension = number_of_element_types - 1

    # ---------------------------------------------------------------------------

    xml_writer.write_header_mesh(ofile, mesh_type_for_elements_dimension[highest_elements_dimension], highest_elements_dimension)
    xml_writer.write_header_vertices(ofile, len(vertices))
    for vertex_number in vertices:
        if dim == 1:
            xml_writer.write_vertex(ofile, vertex_number, vertices[vertex_number][0])
        elif dim == 2:
            xml_writer.write_vertex(ofile, vertex_number, vertices[vertex_number][0], vertices[vertex_number][1])
        else:
            xml_writer.write_vertex(ofile, vertex_number, vertices[vertex_number][0], vertices[vertex_number][1],
                                    vertices[vertex_number][2])
    xml_writer.write_footer_vertices(ofile)

    xml_writer.write_header_cells(ofile, highest_dimension_elements['number_of_elements'])
    for i in highest_dimension_elements:
        if isinstance(i, int):
            if highest_elements_dimension == 3:
                xml_writer.write_cell_tetrahedron(ofile, i, highest_dimension_elements[i]['vertices_used_for_element'][0],
                                                  highest_dimension_elements[i]['vertices_used_for_element'][1],
                                                  highest_dimension_elements[i]['vertices_used_for_element'][2],
                                                  highest_dimension_elements[i]['vertices_used_for_element'][3])
            elif highest_elements_dimension == 2:
                xml_writer.write_cell_triangle(ofile, i, highest_dimension_elements[i]['vertices_used_for_element'][0],
                                               highest_dimension_elements[i]['vertices_used_for_element'][1],
                                               highest_dimension_elements[i]['vertices_used_for_element'][2])
            elif highest_elements_dimension == 1:
                xml_writer.write_cell_interval(ofile, i, highest_dimension_elements[i]['vertices_used_for_element'][0],
                                               highest_dimension_elements[i]['vertices_used_for_element'][1])
    xml_writer.write_footer_cells(ofile)
    xml_writer.write_footer_mesh(ofile)
    # ---------------------------------------------------------------------------

    process_facets = False

    if lower_dimension_elements.get('number_of_markers') > 0:
        try:
            from dolfin import MeshEditor, Mesh
        except ImportError:
            _error("DOLFIN must be installed to handle *.mphtxt boundary regions")
        mesh = Mesh()
        mesh_editor = MeshEditor()
        mesh_editor.open(mesh, highest_elements_dimension, highest_elements_dimension)
        process_facets = True

    if process_facets:
        mesh_editor.init_vertices_global(len(vertices_used_for_highest_dimension_set), len(vertices_used_for_highest_dimension_set))
        for vertex_number in vertices:
            mesh_editor.add_vertex(vertex_number, numpy.array(vertices[vertex_number][:highest_elements_dimension]))
        mesh_editor.init_cells_global(highest_dimension_elements['number_of_elements'], highest_dimension_elements['number_of_elements'])
        for i in highest_dimension_elements:
            if isinstance(i, int):
                mesh_editor.add_cell(i, numpy.array(highest_dimension_elements[i]['vertices_used_for_element'], dtype=numpy.uintp))
        mesh_editor.close()

    # ---------------------------------------------------------------------------

    if not all(marker == 0 for marker in highest_dimension_elements['markers_used_for_elements']):
        xml_writer.write_header_meshfunction(p_ofile, highest_elements_dimension, highest_dimension_elements['number_of_elements'])
        for i in highest_dimension_elements:
            if isinstance(i, int):
                xml_writer.write_entity_meshfunction(p_ofile, i, highest_dimension_elements[i]['marker'])
        xml_writer.write_footer_meshfunction(p_ofile)

    # ---------------------------------------------------------------------------

    if lower_dimension_elements['number_of_markers'] > 0 and mesh is not None:
        if not all(marker == 0 for marker in lower_dimension_elements['markers_used_for_elements']):
            mesh.init(highest_elements_dimension - 1, 0)
            if highest_elements_dimension == 1:
                facets_as_nodes = numpy.array([[i] for i in range(mesh.num_facets())])
            else:
                facets_as_nodes = mesh.topology()(highest_elements_dimension - 1, 0)().reshape(mesh.num_facets(), highest_elements_dimension)

            nodes_as_facets = {}
            for facet in range(mesh.num_facets()):
                nodes_as_facets[tuple(facets_as_nodes[facet, :])] = facet

            data = [int(0*k) for k in range(mesh.num_facets())]


            for i, physical_region in enumerate(lower_dimension_elements['markers_used_for_elements']):
                nodes = [n for n in lower_dimension_elements['vertices_used_for_elements'][highest_elements_dimension * i:(highest_elements_dimension * i + highest_elements_dimension)]]
                nodes.sort()

                if physical_region != 0:
                    try:
                        index = nodes_as_facets[tuple(nodes)]
                        data[index] = physical_region
                    except IndexError:
                        raise Exception ( "The facet (%d) was not found to mark: %s" % (i, nodes))

    # ---------------------------------------------------------------------------

    xml_writer.write_header_meshfunction(f_ofile, highest_elements_dimension-1, mesh.num_facets())
    for index, physical_region in enumerate(data):
        xml_writer.write_entity_meshfunction(f_ofile, index, physical_region)
    xml_writer.write_footer_meshfunction(f_ofile)

    # ---------------------------------------------------------------------------

    ifile.close()
    ofile.close()
    f_ofile.close()
    p_ofile.close()

    print('Conversion done')
    print('Tests:')
    my_dict1 = {i:(lower_dimension_elements['markers_used_for_elements']).count(i) for i in (lower_dimension_elements['markers_used_for_elements'])}
    my_dict2 = {i:data.count(i) for i in data}
    print(my_dict1)
    print(my_dict2)


def _error(message):
    for line in message.split("\n"):
        print("*** %s" % line)
    sys.exit(2)


if __name__ == "__main__":
    ifilename = sys.argv[1] if len(sys.argv) else 'input.mphtxt'
    ofilename = sys.argv[2] if len(sys.argv) else 'output.xml'
    mphtxt2xml(ifilename, ofilename)
