# README #

The project is a source code to the article publishe in... '' If you will find it usefull or it will contribute to your wor we would be gratefull if you will refer the following publication:

* Journal of ...

This README presents basic information how to start the project. For theory, formulations please refer to the mentioned article.

### What is this repository for? ###

* The software presented in this repository allows to simulate the stripline with finite element method. The FEniCS project is used as a backend for FEM formulation and solution (PETsc). To automatially generate tetrahedral meshes a gmesh project was used.
* Version: 1.0PR

### How do I get set up? ###

* The project is assumed to be run on the a Linux box with Ubuntu system.
* The source code is configured throug editing the python source code files.
* Dependencies
 * FEniCS (the code was tested with FEniCS version 2016.2)
 * gmsh
* Demo: in the demos folder you can find ready to start bash scripts which show basic usage

### Contribution guidelines ###

* Code review
* Other guidelines

### Who do I talk to? ###

* The owner of the repo is Bartosz Chaber (bartosz.chaber@ee.pw.edu.pl)